/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-label/iron-label.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-drawer-panel/paper-drawer-panel.js';
import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import('./my-sidebar.js');


class MyRegistration extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
     </style>
      <app-drawer-layout >
      <vaadin-form-layout>

      <vaadin-form-item>
        <label slot="label">First Name</label>
        <input class="full-width" value="Jane">
      </vaadin-form-item>
    
      <vaadin-form-item>
        <label slot="label">Last Name</label>
        <input class="full-width" value="Doe">
      </vaadin-form-item>
    
      <vaadin-form-item>
        <label slot="label">Email</label>
        <input class="full-width" value="jane.doe@example.com">
      </vaadin-form-item>
    
    </vaadin-form-layout>
      </app-drawer-layout>
    `;
  }
 
}

window.customElements.define('my-registration', MyRegistration);
