/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';


import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/iron-image/iron-image.js';

class MySidebar extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
      iron-image {
        border: 1px solid lightblue;
        border-radius: 50%;
      }
    
      .box {
        display: flex;
        align-items:center;
     }


      </style>

      <div >
      <app-header-layout>
      <app-header slot="header">
        <app-toolbar>
          <div main-title>App name</div>
        </app-toolbar>
      </app-header>
 
  
    </app-header-layout>
         <br>

         <div class="box">
         <iron-image  style="width:100px; height:100px; background-color: lightgray;  style="vertical-align:middle"" sizing="cover" src="src/img/myimg.jpg"></iron-image>
       
         <iron-label style="margin-left:10px" ><b>Welcome!!</b><iron-label>
    </div>
         `;
  }
}

window.customElements.define('my-sidebar', MySidebar);
