/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-label/iron-label.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-list/iron-list.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-selection-column.js';
import '@vaadin/vaadin-grid/vaadin-grid-sorter.js';



import('./my-sidebar.js');
class MyView2 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        paper-button.custom:hover {
          background-color: var(--paper-blue-100);
        }
        paper-button.pink {
         color:black;
          background-color: var(--paper-indigo-100);
      
        
        
      </style>
      <app-drawer-layout force-narrow>
      <app-drawer  id="drawer" slot="drawer" swipe-open="" >
      <my-sidebar></my-sidebar>
      </app-drawer>
      <app-toolbar>
      <paper-icon-button icon="menu" on-click="slide_Drawer"></paper-icon-button>
    </app-toolbar>
  
  

      <div class="card">
        <div class="circle">1</div>
        <h1>Account Details     
      
        <paper-icon-button  on-click="ToggleFunc2" icon="add" style=" color: var(--paper-light-blue-500)"></paper-icon-button>
       </h1>
     
       <div>
              <iron-list items="{{array}}" as="item">
                <template>
                  <div>
                  <paper-icon-button on-click="EditEntry" icon="create"   data-args="[[item.key]]" style=" color: var(--paper-light-blue-500); "></paper-icon-button>     
                 <b> [[item.key]]: </b>[[item.value]]
                  
                  
                  </div>

                
                </template>
              </iron-list>
      </div>

      <paper-dialog id="AddDialog" hidden>
              <h5 style="margin-bottom:-10px" >Add Account Details</h5>
              <paper-input style="width:10em " id="mylabel" label = "Enter Property Name" error-message="You must fill in a value" required>
              </paper-input>

              <paper-input style="width:10em " id="myvalue" label = "Enter Property Value" error-message="You must fill in a value" required>
              </paper-input>


              <div class="buttons">
              <paper-button type="submit" class="custom pink " raised on-click="ToggleFunc2" >Cancel</paper-button>
              <paper-button type="submit" class="custom pink " raised autofocus on-click="AddDetail" >Accept</paper-button>
              </div>
  </paper-dialog>


      <paper-dialog id="dialog" hidden>
      <h5 style="margin-bottom:-10px" >Edit Account Details</h5>
      <paper-input style="width:10em; " id="newname" label = "Edit Here" error-message="You must fill in a value" required>
      </paper-input>
      <div class="buttons">

      
      <paper-button dialog-dismiss class="custom pink " raised on-click="ToggleFunc" >Cancel</paper-button>
      <paper-button dialog-confirm  class="custom pink " raised  on-click="UpdateName" >Accept</paper-button>
      </div>
  </paper-dialog>
      </div>


      <div class="card">
      <div class="circle">2</div>
      <h1>Balance Details</h1>
      <iron-label>
      <iron-label>  <paper-icon-button  slot="prefix" icon="create" style=" color: var(--paper-light-blue-500); "></paper-icon-button> <b>Remanining Balance:</b> </iron-label>
      <iron-label><b>PKR</b> 10</iron-label>
    
    </iron-label>

   
    </div>


    <div class="card">
    <div class="circle">3</div>
    <h1>Transaction Details</h1>
    
    
  
     <iron-ajax auto url="src/phones.json" handle-as="json" last-response="{{data}}"></iron-ajax>
     <vaadin-grid  items="[[data]]" as="item">
 
   

    <vaadin-grid-column width="9em">
      <template class="header">
        <vaadin-grid-sorter path="age">ID</vaadin-grid-sorter>
      </template>
      <template>[[item.age]]</template>
    </vaadin-grid-column>

    <vaadin-grid-column width="9em">
      <template class="header">
        <vaadin-grid-sorter path="id">Model</vaadin-grid-sorter>
      </template>
      <template>[[item.id]]</template>
    </vaadin-grid-column>

    <vaadin-grid-column width="20em" flex-grow="2">
      <template class="header">
        <vaadin-grid-sorter path="name">Name</vaadin-grid-sorter>
      </template>
      <template>[[item.name]]</template>
    </vaadin-grid-column>

  </vaadin-grid>
  </div>
  </app-drawer-layout>
    `;
  }
  static get properties() {
    return {
      item: {
        type: String,
        reflectToAttribute: true

      },
      array: {
        type: Array,
        notify: true,
        reflectToAttribute: true,
        value: function() {return new Array();
        }
      },
      selection: {
        type:String,
        value: function() {return "Username"}

      },
      routeData: Object,
      subroute: Object
    };
  }
slide_Drawer()
{
    this.$.drawer.toggle();
}
  ToggleFunc()
  {
    this.$.dialog.hidden=!this.$.dialog.hidden;
    
  }
  ToggleFunc2()
  {
    this.$.AddDialog.hidden=!this.$.AddDialog.hidden;
  }
  
  UpdateName()
  {
    if(this.$.newname.value!="")
    {


      for(var i=0; i<this.array.length;i++)
      {
        if(this.array[i].key==this.selection)
            this.array[i].value=this.$.newname.value;
      }
      var myarray = this.array;
      this.set("array",[]);
      this.$.newname.value="";
      for(var i=0; i<myarray.length;i++)
      {
        this.push('array',  {key: myarray[i].key , value: myarray[i].value });
      }

    }

    this.ToggleFunc();
  }
  AddDetail()
  {
   this.push('array', {key: this.$.mylabel.value , value: this.$.myvalue.value });
   this.$.mylabel.value="";
   this.$.myvalue.value="";
    this.ToggleFunc2();

  }
  ready()
  {
    super.ready();
    var self = this;
    self.push('array', {key:"Username" , value: "Usama Aslam" });
  }
  EditEntry(e)
  {
    console.log(e.target.dataArgs);
    this.selection=e.target.dataArgs;
    this.ToggleFunc();
  }
 
}

window.customElements.define('my-view2', MyView2);
