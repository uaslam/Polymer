/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-a11y-keys/iron-a11y-keys.js';
import '@polymer/paper-button/paper-button.js';
import '@vaadin/vaadin-dialog/vaadin-dialog.js';
import './shared-styles.js';

class MyView1 extends PolymerElement {
  
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        paper-input {
          max-width: 400px;
          margin: auto;
        }
        iron-icon, div[suffix] {
          color: hsl(0, 0%, 50%);
          margin-right: 12px;
        }
        paper-button.custom:hover {
          background-color: var(--paper-blue-100);
        }
        paper-button.pink {
         color:black;
          background-color: var(--paper-indigo-100);
        }
        .dialog-message {
          font: italic 24pt serif;
          background: #fff;
          color: #000;
          padding: 1em;
          border: .25em solid #000;
        }
      </style>
      <app-location route="{{route}}"></app-location>
      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>
      
  
      <div class="card">
      <div style="text-align: center;"> <h1 style="align:center"> Sign In</h1> </div>
        
      <div>
 
    
      <form id="form" method="get" action="/view2">
     
      <iron-a11y-keys id="a11y" target="[[target]]" keys="enter"
      on-keys-pressed="_login"></iron-a11y-keys>
        <paper-input name="email" id="username"  label = "Email" type="email" error-message="Incorrect email format" auto-validate required>
        <iron-icon icon="account-circle" slot="prefix"></iron-icon>
       
        </paper-input>

        </div>
      
      <paper-input name="password" id="password" label = "Password" error-message="This field cannot be empty." type="password" auto-validate required>
       <iron-icon icon="lock-outline" slot="prefix"></iron-icon>
      </paper-input>
    
    <div style="text-align: center; margin-top: 50px; margin-bottom: 30px">
        <paper-button type="submit" on-click="_login" class="custom pink " raised>Login</paper-button> 
     <div>
     
    </form>

    <a style="text-align:center; color:#4285F4;"> Don't have an account? </a>
    <paper-button style="margin-top:20px; margin-left:-10px;">
        <a style="text-align:center; color:#4285F4;" on-click="ToggleDialog">
        <b> <u>Register now!! </u></b></a>
    </paper-button>
    
     </div>


     <vaadin-dialog id="dialog" >
        <template>
 
        <paper-input id="newFname" label = "Enter Name" type="text" auto-validate required>
              <iron-icon icon="account-circle" slot="prefix"></iron-icon>
        </paper-input>
        

        <paper-input id="newAddress" label = "Enter Address" type="text" auto-validate required>
              <iron-icon icon="account-circle" slot="prefix"></iron-icon>
        </paper-input>

        <paper-input id="newEmail" label = "Email" type="email" error-message="Incorrect email format" auto-validate required>
            <iron-icon icon="account-circle" slot="prefix"></iron-icon>
        </paper-input>

      
      <paper-input id="newPassword" label = "Enter Password" error-message="This field cannot be empty." type="password" auto-validate required>
      <iron-icon icon="lock-outline" slot="prefix"></iron-icon>
      </paper-input>
      <paper-input id="password" label = "Confirm Password" error-message="This field cannot be empty." type="password" auto-validate required>
      <iron-icon icon="lock-outline" slot="prefix"></iron-icon>
      </paper-input>

      <div style="text-align: center; margin-top: 50px; margin-bottom: 30px">
        <paper-button type="submit" on-click="SendRequest" class="custom pink " raised>Submit</paper-button> 
      </div>
  

  </template>
</vaadin-dialog>
<iron-ajax  url="http://localhost:8080/login/add" content-type="application/json" method="post" handle-as="json" on-response="onResponse" id="myajax"></iron-ajax>
<iron-ajax  url="http://localhost:8080/login" content-type="application/json" method="post" handle-as="json" on-response="onResponse" id="login"></iron-ajax>
      
    `;
  }
 _login()
 {
  var username= this.$.username.value;
 var password =  this.$.password.value;

  this.$.login.body={"username":username, "password":password};
  this.$.login.headers['Content-Type'] = 'application/x-www-form-urlencoded'; 
  this.$.login.headers['X-Requested-With'] = 'XMLHttpRequest'; 
  this.$.login.headers['Access-Control-Allow-Origin'] = '*';
  this.$.login.url='http://localhost:8080/login';
  this.$.login.generateRequest();
 }
  SendRequest()
  {

    var workOrder=document.querySelector('vaadin-dialog-overlay');
    var content= workOrder.$.overlay.querySelector("#content")
    
    var customer={
      'name' : content.shadowRoot.querySelector("#newFname").value,
      'address': content.shadowRoot.querySelector("#newAddress").value,
      'email': content.shadowRoot.querySelector("#newEmail").value,
      'password': content.shadowRoot.querySelector("#newPassword").value,
      'roles' : 'customer'
    };
    this.$.myajax.body=JSON.stringify(customer);
    
    this.$.myajax.generateRequest();
  }
  ToggleDialog()
  {
    this.$.dialog.opened = !this.$.dialog.opened;
  }
  onResponse (response){

    console.log(response);
  }
  
  SwitchPage()
  {

   
   this.$.loginajax.params={
     username:this.$.email.value,
     password:this.$.password.value
   };
   this.$.loginajax.generateRequest();

}
  
  static get properties() {
    return {
      userInput: {
        type: String,
        notify: true,
      },
      ready: function() {
        this.target = this.$.email;
      },
      target: {
        type: Object
      },
      resolveUrl: {
        type: String
      },
    };
  }

}

window.customElements.define('my-view1', MyView1);
